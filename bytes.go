package goquant

import (
	"bytes"
	"github.com/pkg/errors"
	"os/exec"
)

func CompressBytes(in []byte, opts *PNGQuantOptions) (out []byte, err error) {
	err = ValidateOptions(opts)
	if err != nil {
		return
	}

	cmd := exec.Command(opts.BinaryLocation, opts._commandArgs(true)...)

	cmd.Stdin = bytes.NewReader(in)

	var (
		outBuffer bytes.Buffer
		errBuffer bytes.Buffer
	)

	cmd.Stdout = &outBuffer
	cmd.Stderr = &errBuffer

	err = cmd.Run()
	if err != nil {
		err = errors.New(errBuffer.String())
		return
	}

	out = outBuffer.Bytes()
	return
}
