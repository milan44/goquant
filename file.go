package goquant

import (
	"bytes"
	"errors"
	"os/exec"
)

func CompressFile(in, out string, opts *PNGQuantOptions) (err error) {
	err = ValidateOptions(opts)
	if err != nil {
		return
	}

	args := opts._commandArgs(false)
	args = append(args, "--force", in, "--output", out)

	cmd := exec.Command(opts.BinaryLocation, args...)

	var errBuffer bytes.Buffer
	cmd.Stderr = &errBuffer

	err = cmd.Run()
	if err != nil {
		err = errors.New(errBuffer.String())
	}

	return
}