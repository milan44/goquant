# GoQuant

**GoQuant** is an easy to use wrapper for [pngquant](https://pngquant.org/).

## Usage

To install, use `go get`:

```
$ go get gitlab.com/milan44/goquant
```

### import

```go
import (
    "gitlab.com/milan44/goquant"
)
```

#### func CompressImage

`func CompressImage(in image.Image, opts *PNGQuantOptions) image.Image, error`

#### func CompressBytes

`func CompressBytes(in []byte, opts *PNGQuantOptions) []bytes, error`

#### func CompressFile

`func CompressFile(in, out string, opts *PNGQuantOptions) error`

## License

MIT

## Author

Milan Bartky <http://gitlab.com/milan44>
