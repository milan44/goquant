package goquant

import (
	"bytes"
	"image"
	"image/png"
)

func CompressImage(in image.Image, opts *PNGQuantOptions) (out image.Image, err error) {
	err = ValidateOptions(opts)
	if err != nil {
		return
	}

	var inBuffer bytes.Buffer
	err = png.Encode(&inBuffer, in)
	if err != nil {
		return
	}

	compressed, err := CompressBytes(inBuffer.Bytes(), opts)
	if err != nil {
		return
	}

	out, err = png.Decode(bytes.NewReader(compressed))
	return
}
