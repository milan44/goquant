package goquant

import (
	"github.com/pkg/errors"
	"os"
	"strconv"
)

type PNGQuantOptions struct {
	BinaryLocation string
	Quality        int
	Speed          int
	IEBug          bool
}

func ValidateOptions(opts *PNGQuantOptions) error {
	if opts == nil {
		opts = &PNGQuantOptions{}
		return nil
	}

	if opts.Quality < 0 || opts.Quality > 100 {
		return errors.New("invalid quality option (0-100)")
	} else if opts.Speed < 0 || opts.Speed > 10 {
		return errors.New("invalid speed option (1-10)")
	} else if opts.BinaryLocation != "" {
		if _, err := os.Stat(opts.BinaryLocation); os.IsNotExist(err) {
			return errors.New("binary not found")
		}
	}

	if opts.BinaryLocation == "" {
		opts.BinaryLocation = "pngquant"
	}

	return nil
}

func (o *PNGQuantOptions) _commandArgs(useStdIn bool) []string {
	args := make([]string, 0)

	if useStdIn {
		args = append(args, "-")
	}

	if o.Quality != 0 {
		args = append(args, "--quality", strconv.Itoa(o.Quality))
	}
	if o.Speed != 0 {
		args = append(args, "--speed", strconv.Itoa(o.Speed))
	}
	if o.IEBug {
		args = append(args, "--iebug")
	}

	return args
}
